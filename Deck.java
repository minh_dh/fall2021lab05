import java.util.ArrayList;

public class Deck {
    private ArrayList<Card> cards = new ArrayList<Card>();

    public Deck() {
        for (int i = 0; i < 4; i++) {
            cards.add(new WildCard());
            cards.add(new WildPick4Card());
        }
        for (Color color : Color.values()) {
            for (int i = 1; i < 10; i++) {
                cards.add(new NumberCard(color, i));
                cards.add(new NumberCard(color, i));
            }
            for (int i = 0; i < 2; i++) {
                cards.add(new SkipCard(color));
                cards.add(new ReverseCard(color));
                cards.add(new Pick2Card(color));
            }
            cards.add(new NumberCard(color, 0));
        }
    }

    public void addToDeck(Card card) {
        cards.add(0, card);
    }

    public Card draw() {
        Card drawnCard = cards.get(cards.size() - 1);
        cards.remove(cards.size() - 1);
        return drawnCard;
    }

    public ArrayList<Card> getArrayList() {
        return this.cards;
    }
}
