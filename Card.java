public interface Card {
    Boolean canPlay(Card lastPlayed);

    Color getColor();
}
