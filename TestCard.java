import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestCard {

    @Test
    void testNumberCardConstructor() {
        Deck deck = new Deck();
        assertEquals(108, deck.getArrayList().size());
    }

    @Test
    void testAddToDeck() {
        Deck deck = new Deck();
        deck.addToDeck(new NumberCard(Color.Green, 10));

        NumberCard testCard = (NumberCard) deck.getArrayList().get(0);
        assertEquals(Color.Green, testCard.getColor());
        assertEquals(10, testCard.getNumber());
    }

    @Test
    void testDraw() {
        Deck deck = new Deck();
        NumberCard testCard = (NumberCard) deck.draw();

        assertEquals(Color.Blue, testCard.getColor());
        assertEquals(0, testCard.getNumber());
    }

    @Test
    void testCanPlay() {
        Card wild = new WildCard();
        Card wildPick4 = new WildPick4Card();
        Card pick2Blue = new Pick2Card(Color.Blue);
        Card reverseBlue = new ReverseCard(Color.Blue);
        Card skipRed = new SkipCard(Color.Red);
        Card blue2 = new NumberCard(Color.Blue, 2);
        Card green2 = new NumberCard(Color.Green, 2);
        Card green3 = new NumberCard(Color.Green, 3);
        Card red1 = new NumberCard(Color.Red, 1);

        assertTrue(wild.canPlay(blue2));

        assertTrue(wildPick4.canPlay(pick2Blue));

        assertTrue(pick2Blue.canPlay(blue2));
        assertFalse(pick2Blue.canPlay(green2));

        assertTrue(reverseBlue.canPlay(blue2));
        assertFalse(reverseBlue.canPlay(red1));

        assertTrue(skipRed.canPlay(red1));
        assertFalse(skipRed.canPlay(blue2));

        assertTrue(blue2.canPlay(pick2Blue));
        assertTrue(blue2.canPlay(green2));
        assertFalse(blue2.canPlay(green3));
    }
}
