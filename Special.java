public abstract class Special implements Card {
    protected Color color;

    public Special(Color color) {
        this.color = color;
    }

    public Boolean canPlay(Card lastPlayed) {
        if (this.color == lastPlayed.getColor()) {
            return true;
        } else {
            return false;
        }
    }

    public Color getColor() {
        return this.color;
    }
}
