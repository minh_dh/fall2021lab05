public class NumberCard implements Card {
    private Color color;
    private int number;

    NumberCard(Color color, int number) {
        this.color = color;
        this.number = number;
    }

    public Boolean canPlay(Card lastPlayed) {
        if (this.color == lastPlayed.getColor()) {
            return true;
        } else if (lastPlayed instanceof NumberCard) {
            NumberCard lastNumberCard = (NumberCard) lastPlayed;
            if (this.number == lastNumberCard.getNumber()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public Boolean compareNumber(NumberCard lastPlayed) {
        if (this.number == lastPlayed.getNumber()) {
            return true;
        } else {
            return false;
        }
    }

    public Color getColor() {
        return this.color;
    }

    public int getNumber() {
        return this.number;
    }
}
