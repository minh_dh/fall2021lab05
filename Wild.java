public abstract class Wild implements Card {
    protected Color color;

    public Boolean canPlay(Card lastPlayed) {
        return true;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return this.color;
    }
}
